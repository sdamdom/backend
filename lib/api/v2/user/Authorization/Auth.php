<?php

namespace api\v2\user\Authorization;

use api\Reader\Read;
use api\JsonEncode\Encode;
use Respect\Validation\Validator as v;
use \RedBeanPHP\R as R;

class Auth
{
    private $login;
    private $password;


    /**
     * @param $data
     * @param $encode
     */
    public function __construct(Read $data)
    {
        if (!array_key_exists('login', $data->authData)) {
            throw new \RuntimeException('Login отсутствует', UNAUTHORIZED);
        }
        if (!array_key_exists('password', $data->authData)) {
            throw new \RuntimeException('Пароль отсутствует', UNAUTHORIZED);
        }
        $this->login = $data->authData['login'];
        $this->password = $data->authData['password'];

    }

    /**
     * @return array
     * @throws \Exception
     */
    public function login(Encode $encode)
    {
        $this->loginValidate();
        $encode->default = $this->doLogin();
    }

    /**
     * @throws \Exception
     */
    public function loginValidate()
    {

        if (!v::email()->validate($this->login)) {
            throw new \RuntimeException('login is not e-mail', ACCESS_DENIED);
        }

        return true;

    }

    /**
     * @return array
     * @throws \Exception
     */
    public function doLogin()
    {
        $user  = R::findOne( 'users', ' login = ? ', [ $this->login ] );
        $ok = password_verify($this->password, $user['password']);
        if (!$ok) {
            throw new \RuntimeException('Wrong password or Email not found', ACCESS_DENIED);
        }
        $result = ['id' => $user->id,'login' => $user->login, '1name' => $user->first_name, '2name' => $user->last_name];
        return $result;
    }
}