<?php
namespace api\v2\user\Markers;

use \RedBeanPHP\R as R;
use api\Reader\Read;
use api\JsonEncode\Encode;
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 30.05.2018
 * Time: 23:28
 */
class GetMarkers {
    public $filter;
    public $tableName = 'chijutid7vnvuuerqlewhm_a0yg';
    public $typeDealId = 1;
    public $typeFloatId = 3;
    public $id = array();

    public function getMarkers(Encode $encode) {
        $this->verifyFilter();
        $this->getIds();
        $encode->default = $this->id;
        return true;
    }

    public function verifyFilter() {
        return true;
    }

    public function getIds() {
        $ids = R::findLike( $this->tableName, [
            'type_deal_id' => $this->typeDealId, 'type_float_id' => $this->typeFloatId
        ], ' ORDER BY id  ' );
        foreach ($ids as $obj) {
           // echo $obj->id;
            $this->id[] = ['id'=>$obj->id,'lat'=>$obj->lat,'lng'=>$obj->lng];
        }
        return true;
    }
}