<?php
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 17.05.2018
 * Time: 23:48
 */

namespace api\MyJson;
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 08.05.2018
 * Time: 0:06
 */
class Json
{
    public $returnData;
    public $array;

    public function __construct($data)
    {
        $this->array = $data;
    }

    public function getJsonEncodeData($data)
    {
        $this->returnData = json_encode($data);
    }
}