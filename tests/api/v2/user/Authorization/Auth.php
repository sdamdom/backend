<?php

namespace api\v2\user\Authorization;

use Respect\Validation\Validator as v;

class Auth
{
    private $login;
    private $password;

    /**
     * @param $data
     * @throws \Exception
     */
    public function __construct($data)
    {
        if (!array_key_exists('login', $data->authData)) {

        }
        if (!array_key_exists('password', $data->authData)) {
            throw new \RuntimeException('Пароль отсутствует', UNAUTHORIZED);
        }
        $this->login = $data->authData['login'];
        $this->password = $data->authData['password'];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function login()
    {
        $this->loginValidate();
        return $this->doLogin();
    }

    /**
     * @throws \Exception
     */
    public function loginValidate()
    {

        if (!v::email()->validate($this->login)) {
            throw new \RuntimeException('login is not e-mail', ACCESS_DENIED);
        }

        return true;

    }

    /**
     * @return array
     * @throws \Exception
     */
    public function doLogin()
    {
        $ok = $this->login === 'test@mail.ru' && $this->password === 'qwe123';
        if (!$ok) {
            throw new \RuntimeException('Вы ввели неверный логин/пароль', ACCESS_DENIED);
        }
        $result = ['id' => 2, 'name' => 'ivan', 'country' => 'Russia', 'office' => ['yandex', 'management']];
        return $result;
    }
}