<?php
namespace api\v2\user\Objects;
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 22.05.2018
 * Time: 22:24
 */

class ObjectList {
    private $id;
    private $data;
    private $authData;

    /**
     * Object constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->authData = $data->authData;
        $this->data = [
            ['id'=>0,'city'=>'Дзержинск','somevalue'=>'somevalue'],
            ['id'=>1,'city'=>'Нижний Новгород','somevalue'=>'somevalue'],
            ['id'=>2,'city'=>'Санкт-Петербург','somevalue'=>'somevalue'],
            ['id'=>3,'city'=>'Санкт-Петербург','somevalue'=>'somevalue'],
            ['id'=>4,'city'=>'Дзержинск','somevalue'=>'somevalue']
        ];
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getOne() {
        $this->verifyId();
        if (!isset($this->data[$this->id])) {
            throw new \RuntimeException('Object with this ID not found', UNAUTHORIZED);
        }
        return $this->data[$this->id];
    }

    /**
     *
     * @throws \Exception
     */
    public function verifyId() {
        if (empty($this->authData['id']))
        {
            throw new \RuntimeException('Enter ID', UNAUTHORIZED);
        }
        $this->id = $this->authData['id'];
    }

    /**
     * @return mixed
     */
    public function getList() {
        //Делаем выборку из БД по входным данным
        return $this->authData;
    }
}
class ObjectListFave {

}